#TornadoFX Chess
I am creating this chess game program as a side-project to teach myself TornadoFX.
This is a very simple game that I intend to have all logic and functionality that is expected of chess.
In the future I am looking into making this able to be accessed through a web server to have an AI to challenge.


##Getting Started
###Prerequisties
* Kotlin
* Maven
* Java
* Intellij IDEA or similar IDE

###Installing
1. Clone this repository
2. Open the repository in a Kotlin and Maven compatible IDE
3. Import all Maven sources
4. Run the program


##Built With
* Kotlin - The primary language used
* TornadoFX - UI framework for Kotlin
* Maven - Dependency Management

##Authors
* Luke Parkinson - Sole Developer