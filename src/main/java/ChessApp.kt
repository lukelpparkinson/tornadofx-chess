import tornadofx.*
import view.BoardView
import view.Styles

fun main(args: Array<String>) = launch<ChessApp>(args)

class ChessApp : App(BoardView::class, Styles::class) {
    init {
        reloadStylesheetsOnFocus()
    }
}