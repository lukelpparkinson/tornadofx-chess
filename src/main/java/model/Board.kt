package model

import model.piece.*
import java.util.*

class Board private constructor() : Observable() {
    private var initialized = false
    private var _currentTurn: Color = Color.WHITE

    val currentTurn: Color
        get() = _currentTurn

    var pieces: Set<ChessPiece> = setOf()

    fun findPiece(position: Position): ChessPiece? = pieces.findLast {
        it.position == position
    }

    fun switchTurns() {
        _currentTurn = if (_currentTurn == Color.WHITE)
            Color.BLACK
        else
            Color.WHITE
        setChanged()
        notifyObservers(_currentTurn.color)
    }

    companion object {
        private val instance: Board = Board()

        fun getInstance(): Board {
            if (!instance.initialized) {
                initializeBoard()
                instance.initialized = true
            }
            return instance
        }

        private fun initializeBoard() {
            val pieces: MutableSet<ChessPiece> = mutableSetOf()
            for (i in 0..7) {
                pieces += Pawn(Color.BLACK, Position(i, 1))
                pieces += Pawn(Color.WHITE, Position(i, 6))
            }
            pieces += Queen(Color.BLACK, Position(3, 0))
            pieces += Queen(Color.WHITE, Position(3, 7))

            pieces += King(Color.BLACK, Position(4, 0))
            pieces += King(Color.WHITE, Position(4, 7))

            pieces += Bishop(Color.BLACK, Position(2, 0))
            pieces += Bishop(Color.BLACK, Position(5, 0))
            pieces += Bishop(Color.WHITE, Position(2, 7))
            pieces += Bishop(Color.WHITE, Position(5, 7))

            pieces += Knight(Color.BLACK, Position(1, 0))
            pieces += Knight(Color.BLACK, Position(6, 0))
            pieces += Knight(Color.WHITE, Position(1, 7))
            pieces += Knight(Color.WHITE, Position(6, 7))

            pieces += Rook(Color.BLACK, Position(0, 0))
            pieces += Rook(Color.BLACK, Position(7, 0))
            pieces += Rook(Color.WHITE, Position(0, 7))
            pieces += Rook(Color.WHITE, Position(7, 7))

            instance.pieces += pieces
        }
    }
}