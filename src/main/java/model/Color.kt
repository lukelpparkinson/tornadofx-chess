package model

enum class Color(val color: String) {
    BLACK("Black"),
    WHITE("White")
}