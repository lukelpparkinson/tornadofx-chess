package model

import model.piece.ChessPiece
import kotlin.math.sign

abstract class Movable {
    open fun move(position: Position, piece: ChessPiece): Boolean {
        if (findMovablePositions(piece).contains(position)) {
            piece.position = position
            return true
        }
        return false
    }

    open fun findMovablePositions(piece: ChessPiece): Set<Position> {
        val positions = Position.getAllPositions()

        return positions
            .filter {
                it != piece.position
            }
            .filter {
                val pieceInSquare: ChessPiece? = Board.getInstance().findPiece(it)
                if (pieceInSquare == null) {
                    true
                } else
                    pieceInSquare.color != piece.color
            }.toSet()
    }

    protected fun checkNotJump(current: Position, possible: Position): Boolean {
        val path: List<Position> = path(current, possible)
        return path.all {
            Board.getInstance().findPiece(it) == null
        }
    }

    private fun path(current: Position, possible: Position): List<Position> {
        if (current == possible) return listOf()
        val signX = (current.x - possible.x).sign
        val signY = (current.y - possible.y).sign
        return (path(current, Position(possible.x + signX, possible.y + signY)) + possible)
    }
}