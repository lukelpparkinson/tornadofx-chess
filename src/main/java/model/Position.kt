package model


data class Position(val x: Int, val y: Int) {

    init {
        if (x !in 0..7 || y !in 0..7)
            throw IllegalArgumentException("Illegal position. Must be in range 0..7")
    }

    fun isBlackSquare(): Boolean = ((x % 2 == 0) xor (y % 2 == 0))


    companion object {
        fun getAllPositions(): Set<Position> {
            val positions = mutableSetOf<Position>()
            for (i in 0..7) {
                for (j in 0..7) {
                    positions.add(Position(i, j))
                }
            }
            return positions
        }
    }

}
