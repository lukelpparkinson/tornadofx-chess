package model.behaviour

import model.Movable
import model.Position
import model.piece.ChessPiece

class KingBehaviour : Movable() {
    override fun findMovablePositions(piece: ChessPiece): Set<Position> {
        return super.findMovablePositions(piece).filter {
            it.x in piece.position.x - 1..piece.position.x + 1 &&
                    it.y in piece.position.y - 1..piece.position.y + 1
        }.toSet()
    }
}