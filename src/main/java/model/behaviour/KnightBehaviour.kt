package model.behaviour

import model.Movable
import model.Position
import model.piece.ChessPiece

class KnightBehaviour : Movable() {
    override fun findMovablePositions(piece: ChessPiece): Set<Position> {
        return super.findMovablePositions(piece).filter {
            (it.x in setOf(piece.position.x - 1, piece.position.x + 1) &&
                    it.y in setOf(piece.position.y - 2, piece.position.y + 2))
            ||
            (it.x in setOf(piece.position.x - 2, piece.position.x + 2) &&
                            it.y in setOf(piece.position.y - 1, piece.position.y + 1))
        }.toSet()
    }
}