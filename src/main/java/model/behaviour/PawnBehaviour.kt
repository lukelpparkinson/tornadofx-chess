package model.behaviour

import model.Color
import model.Movable
import model.Position
import model.piece.ChessPiece

// TODO: En Passant
// TODO: Promotion
class PawnBehaviour : Movable() {
    private var firstMove = true

    override fun move(position: Position, piece: ChessPiece): Boolean {
        return super.move(position, piece).also { if (it) firstMove = false }
    }

    override fun findMovablePositions(piece: ChessPiece): Set<Position> {
        return super.findMovablePositions(piece).filter {
            var res = (it.x == piece.position.x)
            res = res && checkNotJump(piece.position, it)
            when (piece.color) {
                // todo add attack option
                Color.BLACK -> {
                    res && (it.y == piece.position.y + 1 || if (firstMove) it.y == piece.position.y + 2 else firstMove)
                }
                Color.WHITE -> {
                    res && (it.y == piece.position.y - 1 || if (firstMove) it.y == piece.position.y - 2 else firstMove)
                }
            }
        }.toSet()
    }
}