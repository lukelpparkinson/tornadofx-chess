package model.behaviour

import model.Movable
import model.Position
import model.piece.ChessPiece
import kotlin.math.abs

class QueenBehaviour : Movable() {
    override fun findMovablePositions(piece: ChessPiece): Set<Position> {
        return super.findMovablePositions(piece).filter {
            val dx = abs(it.x - piece.position.x)
            val dy = abs(it.y - piece.position.y)
            (dx == dy || dx == 0 || dy == 0) && checkNotJump(piece.position, it)
        }.toSet()
    }

}