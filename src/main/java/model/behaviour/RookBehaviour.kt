package model.behaviour

import model.Movable
import model.Position
import model.piece.ChessPiece

// TODO: Castling
class RookBehaviour : Movable() {
    override fun findMovablePositions(piece: ChessPiece): Set<Position> {
        return super.findMovablePositions(piece).filter {
            (it.x == piece.position.x || it.y == piece.position.y) &&
                    checkNotJump(piece.position, it)
        }.toSet()
    }
}