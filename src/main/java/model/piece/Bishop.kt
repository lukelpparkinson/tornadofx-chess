package model.piece

import model.Color
import model.Movable
import model.Position
import model.behaviour.BishopBehaviour

class Bishop(
    override val color: Color,
    override var position: Position
) : ChessPiece() {

    override val spriteUrl: String =
        if (color == Color.BLACK) "b_bishop.png" else "w_bishop.png"

    override val moveBehaviour: Movable = BishopBehaviour()
}