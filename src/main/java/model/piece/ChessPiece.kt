package model.piece

import model.Color
import model.Movable
import model.Position

abstract class ChessPiece : Movable() {
    abstract val color: Color
    abstract var position: Position
    abstract val spriteUrl: String
    abstract val moveBehaviour: Movable

    fun move(position: Position) = moveBehaviour.move(position, this)

    override fun findMovablePositions(piece: ChessPiece): Set<Position> = moveBehaviour.findMovablePositions(piece)
    fun findMovablePositions(): Set<Position> = findMovablePositions(this)
}