package model.piece

import model.Color
import model.Movable
import model.Position
import model.behaviour.KingBehaviour

class King(
    override val color: Color,
    override var position: Position
) : ChessPiece() {
    override val spriteUrl: String =
        if (color == Color.BLACK) "b_king.png" else "w_king.png"

    override val moveBehaviour: Movable = KingBehaviour()
}