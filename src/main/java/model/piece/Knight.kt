package model.piece

import model.Color
import model.Movable
import model.Position
import model.behaviour.KnightBehaviour

class Knight(
    override val color: Color,
    override var position: Position
) : ChessPiece() {
    override val spriteUrl: String =
        if (color == Color.BLACK) "b_knight.png" else "w_knight.png"

    override val moveBehaviour: Movable = KnightBehaviour()
}