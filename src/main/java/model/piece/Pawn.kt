package model.piece

import model.Color
import model.Movable
import model.Position
import model.behaviour.PawnBehaviour

class Pawn(
    override val color: Color,
    override var position: Position
) : ChessPiece() {
    override val spriteUrl: String =
        if (color == Color.BLACK) "b_pawn.png" else "w_pawn.png"

    override val moveBehaviour: Movable = PawnBehaviour()
}