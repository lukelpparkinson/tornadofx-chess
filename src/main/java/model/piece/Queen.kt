package model.piece

import model.Color
import model.Movable
import model.Position
import model.behaviour.QueenBehaviour

class Queen(
    override val color: Color,
    override var position: Position
) : ChessPiece() {
    override val spriteUrl: String =
        if (color == Color.BLACK) "b_queen.png" else "w_queen.png"

    override val moveBehaviour: Movable = QueenBehaviour()
}