package model.piece

import model.Color
import model.Movable
import model.Position
import model.behaviour.RookBehaviour

class Rook(
    override val color: Color,
    override var position: Position
) : ChessPiece() {
    override val spriteUrl: String =
        if (color == Color.BLACK) "b_rook.png" else "w_rook.png"

    override val moveBehaviour: Movable = RookBehaviour()
}