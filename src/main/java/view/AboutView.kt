package view

import javafx.scene.control.Label
import model.Board
import tornadofx.*
import java.awt.Desktop
import java.net.URI
import java.util.*

class AboutView : View(), Observer {
    private val currentTurnLabel = Label()

    override val root = vbox {
        this += currentTurnLabel
        label {
            text = "Attributions:"
        }
        hbox {
            hyperlink {
                text = "ChessPieces.png"
                setOnAction {
                    Desktop.getDesktop().browse(URI("https://commons.wikimedia.org/wiki/File:ChessPiecesArray.png"))
                }
            }

            label { text = "by Wikipedia user: Cburnett" }

            hyperlink {
                text = "[CC BY-SA 3.0]"
                setOnAction {
                    Desktop.getDesktop().browse(URI("https://creativecommons.org/licenses/by-sa/3.0"))
                }
            }
        }
    }

    init {
        val board: Board = Board.getInstance()
        board.addObserver(this)
        currentTurnLabel.text = "${board.currentTurn.color}'s turn"
    }

    override fun update(o: Observable?, arg: Any?) {
        if (o is Board && arg is String) {
            currentTurnLabel.text = "$arg's turn"
        }
    }
}
