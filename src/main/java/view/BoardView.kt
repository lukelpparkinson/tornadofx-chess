package view

import javafx.event.EventHandler
import javafx.scene.Parent
import javafx.scene.control.Label
import javafx.scene.image.Image
import javafx.scene.image.ImageView
import javafx.scene.input.MouseEvent
import javafx.scene.layout.*
import model.Board
import model.Position
import model.piece.ChessPiece
import tornadofx.*


class BoardView : View("Chess") {
    override val root = HBox()
    private val boardPane = GridPane()
    private val about = AboutView()

    private val board: Board = Board.getInstance()
    private val positions: MutableMap<Position, ImageView> = mutableMapOf()

    private var selectedSquare: Position? = null
    private val possibleSquares: MutableSet<Position> = mutableSetOf()
    private var selectedPiece: ChessPiece? = null

    init {
        root += boardPane
        root += about
        boardPane.drawCheckerBoard()
        initPieces()
    }

    private fun GridPane.drawCheckerBoard() {
        isGridLinesVisible = true
        maxHeight = -infinity.value
        minHeight = -infinity.value
        minWidth = -infinity.value
        for (i in 0..7) {
            columnConstraints += ColumnConstraints(100.0)
            rowConstraints += RowConstraints(100.0)
        }
        for (i in 0..7) {
            for (j in 0..7) {
                val position = Position(i, j)
                val img: ImageView = imageview {
                    fitWidth = 100.0
                    fitHeight = 100.0
                    image = getDefaultSquareImage()
                }
                val pane: TilePane = tilepane {
                    onMouseClicked = clickEvent(position)
                    addClass(getSquareColor(position))
                }.gridpaneConstraints {
                    columnRowIndex(i, j)
                }
                pane += img
                this += pane
                positions[position] = img
            }
        }
    }

    private fun initPieces() {
        for (piece: ChessPiece in board.pieces) {
            positions[piece.position]?.apply {
                image = Image(piece.spriteUrl)
            }
        }
    }

    private fun clickEvent(position: Position): EventHandler<in MouseEvent>? {
        return EventHandler {
            clearSelectedSquare()
            clearPossibleSquares()

            if (selectedPiece == null) {
                setSelectedSquare(position)
            } else {
                selectedPiece?.let {
                    attemptMove(it, position)
                }
                selectedPiece = null
            }
        }
    }

    private fun setSelectedSquare(position: Position) {
        clearSelectedSquare()
        clearPossibleSquares()
        val piece: ChessPiece? = board.findPiece(position)

        if (piece != null && piece.color == board.currentTurn) {
            this.selectedPiece = piece
            addClassToPosition(position, Styles.selected)
            selectedSquare = position
            val movablePositions: Set<Position> = piece.findMovablePositions()
            this.possibleSquares.addAll(movablePositions)

            for (movablePosition: Position in movablePositions) {
                addClassToPosition(movablePosition, Styles.possibleSquare)
            }
        }
    }

    private fun addClassToPosition(position: Position, cssRule: CssRule) {
        positions[position]?.parent?.addClass(cssRule)
    }

    private fun clearSquare(pos: Position) {
        val parentPane: Parent? = positions[pos]?.parent
        parentPane?.removeClass(Styles.selected)
        parentPane?.removeClass(Styles.possibleSquare)
    }

    private fun clearSelectedSquare() {
        selectedSquare?.let {
            clearSquare(it)
        }
    }

    private fun clearPossibleSquares() = possibleSquares.forEach { clearSquare(it) }

    private fun attemptMove(selectedPiece: ChessPiece, position: Position): Boolean {
        if (selectedPiece.color != board.currentTurn) return false
        val startPos: Position = selectedPiece.position
        if (selectedPiece.move(position)) {
            positions[position]?.image = Image(selectedPiece.spriteUrl)
            positions[startPos]?.image = getDefaultSquareImage()

            board.switchTurns()
            return true
        }
        return false
    }

    private fun getDefaultSquareImage(): Image = Image("transparent.png")

    private fun getSquareColor(position: Position): CssRule =
        if (position.isBlackSquare()) {
            Styles.blackBG
        } else {
            Styles.whiteBG
        }
}