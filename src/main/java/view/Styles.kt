package view

import tornadofx.Stylesheet
import tornadofx.c
import tornadofx.cssclass

class Styles : Stylesheet() {
    companion object {
        val blackBG by cssclass()
        val whiteBG by cssclass()
        val selected by cssclass()
        val possibleSquare by cssclass()

        val white = c("white")
        val black = c("#4e5d6d")
        val selectedColor = c("orange")
        val possibleColor = c("deepskyblue")
    }

    init {
        blackBG {
            backgroundColor += black
        }
        whiteBG {
            backgroundColor += white
        }
        selected {
            backgroundColor += selectedColor
        }
        possibleSquare {
            backgroundColor += possibleColor
        }
    }
}